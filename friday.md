# Friday

## Aspects:
* HIGH ASPECT: Hyper-competent Assistant to Idiot Explorers
* TROUBLE: Inherent Limitations Everywhere
* BACKGROUND: At Least *I* Survived
* GUEST STAR: (Jericho) Research is Good for the Soul
* GUEST STAR: (Will) Be Smart, Play Dumb

##### Current Space Points: 1 / infinitem
##### Chronicle Log Space Points: 2 / 3


## Temporary Aspects:

Used | Aspect Name
-----|--------------------------------
 [ ] | 
 [ ] | 
 [ ] | 


## Skills:

Rating | Skill
------ | --------
  +?   | Science
  +?   | Security
  +?   | Bureaucracy
  +?   | Empathy
  +?   | Deceive
  +?   | Will
  +?   | Investigate
  +?   | Academics

  
##### Skill Points Available: ??
##### Skill Points Spent: ??


## Consequences:

RANK     | ASPECT
---------|--------
Mild     | 
Moderate | 
Severe   | 


STRESS TRACKS | AVAILABLE
--------------|----------------
MENTAL        | [ ] [ ] [ ]
PHYSICAL      | [ ] [ ] [ ]



## Stunts:
* Goddamn AI: While you may not roll social without a penalty unless you have another stunt enabling you to do so, in terms of sheer logic power, you act incredibly fast.  In a race against normal fleshies, you win.  (AI Speed I)
* Well-Read Social Skills: During play age, you were given communication logs of socially compentent people.  You may roll Deceive and Empathy normally.
* From the Shoulders of Giants (Blantant Plagerism): Jericho is a terrible influence.  If you can work with someone else, get a +1 to Science rolls on the subject, and complete your research one time increment faster.


##### Total Refresh:   ?
##### Current Refresh: 1


## Notes:

* Born from a Darwinian contest to avoid Jericho's notice
* Sees Jericho as a shapable force of nature
* Looking forward to working with anyone else for something novel